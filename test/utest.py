from handler import handler
import unittest

# todo assertDictContainsSubset
# import os
# import boto3
# import botocore
# from moto import mock_s3

# os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
# os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
#
# dynamodb = boto3.resource("dynamodb", region_name="eu-west-1")
#
# dynamodb.create_bucket(Bucket='mybucket')
#
# @mock_s3
class TestGet(unittest.TestCase):

    def setUp(self):
        self.get = {
            "httpMethod": "GET",
            "path": "/content",
            "headers": {},
            "queryStringParameters": {"campaign_id": "1"}
            # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
        }
        self.errorget = {
            "httpMethod": "GET",
            "path": "/content",
            "headers": {},
            "queryStringParameters": {"NOcampaign_id": "1"}
            # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
        }

    def test_get_content(self):
        result = handler(self.get, {})
        self.assertEqual(result["statusCode"], '200')

    def test_get_without_id(self):
        result = handler(self.errorget, {})
        # print(result['body'])
        self.assertEqual(result["statusCode"], '400')
        # self.assertEqual(result['body']['error'], "Can't get from campaign content without a 'campaign_id' parameter")
        # self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()

