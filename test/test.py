import handler
import pytest
import unittest

# import os
# import boto3
# import botocore
# from moto import mock_s3

# os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
# os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
#
# dynamodb = boto3.resource("dynamodb", region_name="eu-west-1")
#
# dynamodb.create_bucket(Bucket='mybucket')
#
# @mock_s3
class TestGet(unittest.TestCase):
    def test_get_content(self):
        # # table = dynamodb.Table('campaign_content')
        # # schemas = dynamodb.Table('schemas')
        #
        # table = dynamodb.create_table(
        #     TableName='campaign_content',
        #     KeySchema=[
        #         {
        #             'AttributeName': 'campaign_id',
        #             'KeyType': 'HASH'  # Partition key
        #         },
        #         {
        #             'AttributeName': 'idecran',
        #             'KeyType': 'RANGE'  # Sort key
        #         }
        #     ],
        #     AttributeDefinitions=[
        #         {
        #             'AttributeName': 'campaign_id',
        #             'AttributeType': 'S'
        #         },
        #         {
        #             'AttributeName': 'idecran',
        #             'AttributeType': 'S'
        #         },
        #
        #     ],
        #     ProvisionedThroughput={
        #         'ReadCapacityUnits': 10,
        #         'WriteCapacityUnits': 10
        #     }
        # )
        # schemas = dynamodb.create_table(
        #     TableName='schemas',
        #     KeySchema=[
        #         {
        #             'AttributeName': 'campaign_id',
        #             'KeyType': 'HASH'  # Partition key
        #         }
        #     ],
        #     AttributeDefinitions=[
        #         {
        #             'AttributeName': 'campaign_id',
        #             'AttributeType': 'S'
        #         }
        #
        #     ],
        #     ProvisionedThroughput={
        #         'ReadCapacityUnits': 10,
        #         'WriteCapacityUnits': 10
        #     }
        # )
        get = {
            "httpMethod": "GET",
            "path": "/content",
            "headers": {},
            "queryStringParameters": {"campaign_id": "1"}
            # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
        }
        result = handler.handler(get, {})
        # self.assertFalse(result)
        assert result["statusCode"], "200"




