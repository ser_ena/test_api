import simplejson as json
import routes
import boto3
from boto3.dynamodb.conditions import Key, Attr
import os
import jsonschema
import copy

dynamodb = boto3.resource("dynamodb", region_name="eu-west-1")
table = dynamodb.Table(os.environ["CAMPAIGN_TABLE"])
schemas = dynamodb.Table(os.environ["SCHEMAS_TABLE"])


# local env
# dynamodb = boto3.resource("dynamodb", endpoint_url="http://localhost:8000")
# client = boto3.client('dynamodb', endpoint_url="http://localhost:8000")
# table = dynamodb.Table('campaign_content')
# schemas = dynamodb.Table('schemas')


# schemas = dynamodb.Table('dev-serena_custom_schemas')
# table = dynamodb.Table('dev-serena_custom_campaign_data')


def handler(event, context):
    # add schema to schemas table
    # res = schemas.put_item(Item={"campaign_id": "1", "schema": schema})
    print('event: ', event)

    # upd = table.update_item(Key={"Artist": "Test artist", "SongTitle": "Test title"},
    #                         UpdateExpression="SET AlbumTitle = :a",
    #                         ExpressionAttributeValues={":a": "New album title"})
    # response = table.query(
    #     KeyConditionExpression=Key('Artist').eq('Test artist')
    # )
    # response2 = table.query(
    #     # KeyConditionExpression="#a = :a AND SongTitle = :b",
    #     KeyConditionExpression="#a = :a AND begins_with (SongTitle, :b)",
    #     FilterExpression="AlbumTitle = :s",
    #     ExpressionAttributeNames={'#a': 'Artist'},
    #     ExpressionAttributeValues={':a': 'Test artist', ':b': 'T', ':s': 'New album title'},
    #     # Select='COUNT'
    #     Select='SPECIFIC_ATTRIBUTES',
    #     ProjectionExpression='Awards'
    # )
    path, method, headers, query_string, body = extract_from_event(event)
    # print(path, method, headers, query_string, body)
    mapper = configure_mapper()
    environ = {"REQUEST_METHOD": method}
    map_result = mapper.match(path, environ=environ)
    if map_result is None:
        return format_response("Route not Found", status=404)
    params = {**query_string, **map_result}
    params["values"] = body
    params.pop("action", None)
    try:
        res, h = map_result["action"](**params)
    except CMSException as e:
        res = {'error': str(e)}
        print('error', e)
        return format_response(res, status=400)
    response = {}
    response["response"] = res
    return format_response(response, headers=h)


def extract_from_event(event):
    path = event.get("path", "")
    path = f"/{path.split('/', 2)[-1]}"  # remove base mapping
    method = event.get("httpMethod", "")
    headers = {k.lower(): v for k, v in event["headers"].items()}
    query_string = (
        event["queryStringParameters"]
        if event["queryStringParameters"] is not None
        else {}
    )

    body = {}

    if (
            event["httpMethod"] in ["POST", "PUT"]
            and "content-type" in headers
            and "application/json" in headers["content-type"]
    ):
        body = json.loads(event["body"])
        # body = event["body"]

    return path, method, headers, query_string, body


def configure_mapper():
    mapper = routes.Mapper()
    mapper.connect(
        "cors_content",
        "/content",
        conditions=dict(method=["OPTIONS"]),
        action=options,
    )
    mapper.connect(
        "cors_schema",
        "/schema",
        conditions=dict(method=["OPTIONS"]),
        action=options,
    )

    mapper.connect(
        "get_content",
        "/content",
        conditions=dict(method=["GET"]),
        action=get_content,
    )
    mapper.connect(
        "post_content",
        "/content",
        conditions=dict(method=["POST"]),
        action=post_content,
    )
    mapper.connect(
        "update_content",
        "/content",
        conditions=dict(method=["PUT"]),
        action=update_content,
    )
    mapper.connect(
        "delete_content",
        "/content",
        conditions=dict(method=["DELETE"]),
        action=delete_content,
    )

    mapper.connect(
        "get_schema",
        "/schema",
        conditions=dict(method=["GET"]),
        action=get_schema,
    )
    mapper.connect(
        "post_schema",
        "/schema",
        conditions=dict(method=["POST"]),
        action=post_schema,
    )
    mapper.connect(
        "delete_schema",
        "/schema",
        conditions=dict(method=["DELETE"]),
        action=delete_schema,
    )
    mapper.connect(
        "update_schema",
        "/schema",
        conditions=dict(method=["PUT"]),
        action=update_schema,
    )
    # add new routes here

    return mapper


class CMSException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


def options(**kwargs):
    return {}, {"Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Methods": "OPTIONS,GET,DELETE,POST,PUT"}


def get_content(**kwargs):
    campaign_id = kwargs.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't get from campaign content without a 'campaign_id' parameter")
    idecran = kwargs.get('idecran', 'all')
    try:
        res = table.query(
            KeyConditionExpression="campaign_id = :campaign_id and idecran = :idecran",
            ExpressionAttributeValues={':campaign_id': campaign_id, ':idecran': idecran},
        )
    except Exception as e:
        print(e)
        return False, {}
    if 'Items' in res and len(res['Items']) > 0:
        res['Items'][0].pop("campaign_id", None)
        res['Items'][0].pop("idecran", None)
        return res['Items'][0], {}
    return False, {}


def post_content(**kwargs):
    data = kwargs.get('values', None)
    print(data)
    if data is None:
        raise CMSException("Data error")
    campaign_id = data.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't post without a 'campaign_id' parameter")
    item = copy.deepcopy(data)
    data.pop("campaign_id", None)
    idecran = data.get('idecran', 'all')
    item['idecran'] = idecran
    if idecran is not None:
        data.pop("idecran", None)
    schema, h = get_schema(**{"campaign_id": campaign_id})
    print("schema", schema)
    if schema:
        try:
            print("try validate")
            jsonschema.validate(instance=data, schema=schema)
        except jsonschema.exceptions.ValidationError as e:
            print("well-formed but invalid JSON:", e)
            raise CMSException(e)
        except json.decoder.JSONDecodeError as e:
            print("poorly-formed text, not JSON:", e)
            raise CMSException(e)
        try:
            print("item", item)
            res = table.put_item(Item=item)
            return res, {}
        except Exception as e:
            print(e)
            raise CMSException(e)
    else:
        raise CMSException("Schema not found")


def update_content(**kwargs):
    data = kwargs.get('values', None)
    if data is None:
        return False, {}
    campaign_id = data.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't update without a 'campaign_id' parameter")
    data.pop("campaign_id", None)
    idecran = data.get('idecran', 'all')
    if idecran is not None:
        data.pop("idecran", None)
    schema, h = get_schema(**{"campaign_id": campaign_id})
    if schema:
        try:
            jsonschema.validate(instance=data, schema=schema)
        except jsonschema.exceptions.ValidationError as e:
            print("well-formed but invalid JSON:", e)
            raise CMSException(e)
        except json.decoder.JSONDecodeError as e:
            print("poorly-formed text, not JSON:", e)
            raise CMSException(e)
        # print(data)
        try:
            for item in data:
                upd = table.update_item(Key={"campaign_id": campaign_id, "idecran": idecran},
                                        UpdateExpression="SET #item = :item",
                                        ExpressionAttributeNames={'#item': item},
                                        ExpressionAttributeValues={":item": data[item]},
                                        ConditionExpression="attribute_exists(campaign_id) and attribute_exists(idecran)"
                                        )
        except Exception as e:
            print(e)
            raise CMSException(e)
    else:
        raise CMSException("Schema not found")
    return False, {}


def delete_content(**kwargs):
    campaign_id = kwargs.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't delete without a 'campaign_id' parameter")
    idecran = kwargs.get('idecran', "all")
    try:
        res = table.delete_item(Key={"campaign_id": campaign_id, "idecran": idecran})
    except Exception as e:
        print(e)
        raise CMSException(e)
    return res, {}


def get_schema(**kwargs):
    campaign_id = kwargs.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't get a schema without a 'campaign_id' parameter")
    try:
        res = schemas.get_item(Key={"campaign_id": campaign_id})
    # print(res)
    except Exception as e:
        print(e)
        raise CMSException("Schema not found")
    if 'Item' in res and 'schema' in res['Item']:
        return res['Item']['schema'], {}
    raise CMSException("Schema not found")


def post_schema(**kwargs):
    data = kwargs.get('values', None)
    print(data)
    if data is None:
        raise CMSException("Data error")
    campaign_id = data.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't post a schema without a 'campaign_id' parameter")
    schema = data.get('schema', None)
    print("schema", schema)
    if schema is None:
        raise CMSException("Schema error")
    try:
        res = schemas.put_item(Item={"campaign_id": campaign_id, "schema": schema})
    except Exception as e:
        print(e)
        raise CMSException(e)
    return res, {}
    # print(res)


def update_schema(**kwargs):
    data = kwargs.get('values', None)
    print(data)
    if data is None:
        raise CMSException("Data error")
    campaign_id = data.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't update without a 'campaign_id' parameter")
    new_schema = data.get('schema', None)
    print("schema", new_schema)
    # schema, h = get_schema(**{"campaign_id": campaign_id})
    if new_schema is None:
        raise CMSException("Schema error")
    try:
        # res = schemas.put_item(Item={"campaign_id": campaign_id, "schema": schema})
        upd = schemas.update_item(Key={"campaign_id": campaign_id},
                                  UpdateExpression="SET #item = :item",
                                  ExpressionAttributeNames={'#item': "schema"},
                                  ExpressionAttributeValues={":item": new_schema},
                                  ConditionExpression="attribute_exists(campaign_id)"
                                  )
    except Exception as e:
        print(e)
        raise CMSException(e)
    return upd, {}


def delete_schema(**kwargs):
    campaign_id = kwargs.get('campaign_id', None)
    if campaign_id is None:
        raise CMSException("Can't delete a schema without a 'campaign_id' parameter")
    try:
        res = schemas.delete_item(Key={"campaign_id": campaign_id})
    except Exception as e:
        print(e)
        raise CMSException(e)
    return res, {}


def format_response(content, status=200, headers={}):
    default_headers = {"Access-Control-Allow-Origin": "*"}

    response = {
        "statusCode": str(status),
        "headers": {**default_headers, **headers},
    }

    if content is not None:
        response["body"] = json.dumps(content)
        response["headers"]["Content-Type"] = "application/json"

    return response


if __name__ == "__main__":
    get = {
        "httpMethod": "GET",
        "path": "/content",
        "headers": {},
        # "queryStringParameters": {"campaign_id": "1"}
        "queryStringParameters": {"campaign_id": "1", "idecran": "0001"}
    }
    event = {'path': '/content',
             'httpMethod': 'GET',
             'headers': {
                 'Accept': 'application/json, text/plain, */*'
             },
             'queryStringParameters': {'campaign_id': '1', 'idecran': 'all'},
             'body': None, }

    res = handler(get, {})
    print('GET res', res['body'])

    post = {
        "httpMethod": "POST",
        "path": "/content",
        "headers": {"content-type": "application/json"},
        "queryStringParameters": None,
        "body": {
            "campaign_id": "2",
            "idecran": "2345",
            "matches": 2,
            "results": [55, 66],
            "video": "https://custom-templates-assets.s3-eu-west-1.amazonaws.com/op-redbull-dys-2019/DUO-1.webm"}

    }
    # res1 = handler(post, {})
    # print('POST res', res1['body'])
    put = {
        "httpMethod": "PUT",
        "path": "/content",
        "headers": {"content-type": "application/json"},
        "queryStringParameters": None,
        "body": {
            "campaign_id": "3",
            "idecran": "2345",
            # "matches": 5,
            "results": [1, 2, 3, 4, 5],
            # "video": "https://custom-templates-assets.s3-eu-west-1.amazonaws.com/op-redbull-dys-2019/TRIO-1.webm"
            # "video": "https://custom-templates-assets.s3-eu-west-1.amazonaws.com/op-redbull-dys-2019/DUO-1.webm"
        }
    }
    put2 = {
        "httpMethod": "PUT",
        "path": "/content",
        "headers": {"content-type": "application/json"},
        "queryStringParameters": None,
        "body": {
            "campaign_id": "1",
            "idecran": "2345",
            "matches": "5",
            "results": [55, 55, 55, 55, 55],
            # "video": "https://custom-templates-assets.s3-eu-west-1.amazonaws.com/op-redbull-dys-2019/DUO-1.webm"
        }
    }

    delete = {
        "httpMethod": "DELETE",
        "path": "/content",
        "headers": {"content-type": "application/json"},
        "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
        # "body": {
        #     "campaign_id": "1",
        #     "idecran": "2345",
        #     }
    }
    # event= { 'path': '/content', 'httpMethod': 'DELETE',
    #         'headers': {'Accept': 'application/json, text/plain, */*', },
    #         'queryStringParameters': {'campaign_id': '1', 'idecran': 'all'},
    #         'multiValueQueryStringParameters': {'campaign_id': ['1'], 'idecran': ['all']},
    #         'pathParameters': {'proxy': 'content'}, 'stageVariables': None,
    #         'requestContext': {'resourceId': '2ervmj', 'resourcePath': '/{proxy+}', 'httpMethod': 'DELETE',
    #                            'extendedRequestId': 'A8H4jGJjjoEFqKw=', 'requestTime': '02/Oct/2019:14:53:04 +0000',
    #                            'path': '/dev-serena/content', 'accountId': '779543144853', 'protocol': 'HTTP/1.1',
    #                            'stage': 'dev-serena', 'domainPrefix': 'iamvppmxha', 'requestTimeEpoch': 1570027984291,
    #                            'requestId': 'bf265960-e4e2-4db9-bbb7-c8b999e2a16c',
    #                            'identity': {'cognitoIdentityPoolId': None, 'accountId': None, 'cognitoIdentityId': None,
    #                                         'caller': None, 'sourceIp': '93.12.31.38', 'principalOrgId': None,
    #                                         'accessKey': None, 'cognitoAuthenticationType': None,
    #                                         'cognitoAuthenticationProvider': None, 'userArn': None,
    #                                         'userAgent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36',
    #                                         'user': None},
    #                            'domainName': 'iamvppmxha.execute-api.eu-west-1.amazonaws.com', 'apiId': 'iamvppmxha'},
    #         'body': None, 'isBase64Encoded': False}

    postschema = {
        "httpMethod": "POST",
        "path": "/schema",
        "headers": {"content-type": "application/json"},
        "queryStringParameters": None,
        "body": {
            "campaign_id": "3",
            "schema": {
                "type": "object",
                "title": "Campaign Test 2",
                "properties": {
                    "matches": {
                        "type": "number",
                        "title": "Number of matches"
                    },
                    "results": {
                        "title": "Results",
                        "type": "array",
                        "items": {
                            "type": "number"
                        }
                    },
                    "video": {
                        "title": "video URL",
                        "type": "string",
                        "format": "uri"
                    },
                    "multipleCheckbox": {
                        "type": "array",
                        "anyOf": [
                            {"value": "daily", "label": "Daily New"},
                            {"value": "promotion", "label": "Promotion"}
                        ]
                    }
                }
            }
        }
    }
    # res1 = handler(postschema, {})
    # print('POST res', res1['body'])

    getschema = {
        "httpMethod": "GET",
        "path": "/schema",
        "headers": {},
        "queryStringParameters": {"campaign_id": "1"}
    }
    # res1 = handler(getschema, {})
    # print('GET SCHEMA', res1['body'])

    # res1 = handler(put, {})
    # res1 = handler(put2, {})
    # print('PUT res', res1['body'])
    # r = handler(delete, {})
    res = handler(get, {})
    print('GET res', res['body'])
    exit(0)
