from .handler import handler
import pytest
import unittest.mock
from unittest.mock import MagicMock
from unittest.mock import Mock

# import os
# import boto3
# import botocore
from moto import mock_s3

# os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
# os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'

# dynamodb = boto3.resource("dynamodb", region_name="eu-west-1")
# dynamodb.create_bucket(Bucket='mybucket')


from moto.dynamodb2 import dynamodb_backend2, mock_dynamodb2

import boto3
from moto import mock_dynamodb2

#
# @mock_s3
# def test_my_model_save():
#     dynamodb = boto3.resource("dynamodb", region_name="eu-west-1")
#     # We need to create the bucket since this is all in Moto's 'virtual' AWS account
#     conn.create_bucket(Bucket='mybucket')
#
#     model_instance = MyModel('steve', 'is awesome')
#     model_instance.save()
#
#     body = conn.Object('mybucket', 'steve').get()['Body'].read().decode("utf-8")
#
#     assert body == 'is awesome'


@pytest.fixture()
def dynamodb_table():
    with mock_dynamodb2():
        dynamodb_backend2.create_table("campaign_content", schema=[
            {u'KeyType': u'HASH', u'AttributeName': u'campaign_id'},
            {u'KeyType': u'RANGE', u'AttributeName': u'idecran'}])

        yield "campaign_content"

def test_example(dynamodb_table):
    assert dynamodb_table in dynamodb_backend2.tables


@pytest.fixture()
def get():
    return {
        "httpMethod": "GET",
        "path": "/content",
        "headers": {},
        "queryStringParameters": {"campaign_id": "1"}
        # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
    }


# @pytest.yield_fixture()
# def simple_comme_bonjour():
#     # tout ce qui est setup() va au dessus du yield. Ca peut etre vide.
#     print('Avant !')
#
#     # Ce qu'on yield sera le contenu du parametre. Ca peut etre None.
#     yield ('pomme', 'banane')
#
#     # Ce qu'il y a apres le yield est l'equivalent du tear down et peut être
#     # vide aussi
#     print('Apres !')
#

def test_get(get):

    # get = {
    #     "httpMethod": "GET",
    #     "path": "/content",
    #     "headers": {},
    #     "queryStringParameters": {"campaign_id": "1"}
    #     # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
    # }
    # mock.mock = MagicMock(return_value="moooooooooock")
    # print(mock)
    res = handler(get, {})
    assert res['statusCode'] == '200'


def test_get_no_campaign_id():
    get = {
        "httpMethod": "GET",
        "path": "/content",
        "headers": {},
        "queryStringParameters": {"nocampaign_id": "1"}
        # "queryStringParameters": {"campaign_id": "2", "idecran": "2345"}
    }
    res = handler(get, {})
    assert res['statusCode'] == '400'
