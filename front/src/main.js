import Vue from 'vue'
import App from './App.vue'
import router from './router'
Vue.config.productionTip = false

import axios from 'axios';
const baseURL = 'https://iamvppmxha.execute-api.eu-west-1.amazonaws.com/dev-serena/';
Vue.prototype.$axios = axios.create({ baseURL });

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
