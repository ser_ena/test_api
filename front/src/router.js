import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Contents from '@/components/Contents'
import Schemas from '@/components/Schemas'
Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/contents',
      name: 'Contents',
      component: Contents
    },
    {
      path: '/schemas',
      name: 'Schemas',
      component: Schemas
    },
    // {
    //   path: "*",
    //   name: 'PageNotFound',
    //   component: PageNotFound
    // },
  ],
  linkActiveClass: 'is-active'
})