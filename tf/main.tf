provider "aws" {
  region = "${var.region}"
}

provider "aws" {
  alias   = "acm-certs"
  region  = "us-east-1"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "elise-terraform-states"
    region = "eu-west-1"
    key = "states/helloWorld"
  }
}