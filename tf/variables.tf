variable "region" {
  type    = "string"
  default = "eu-west-1"
}

variable "app_name" {
  type    = "string"
}

variable "func_bucket" {
  type    = "string"
}

variable "func_version_helloWorld" {
  type    = "string"
}

variable "func_name_helloWorld" {
  type    = "string"
  default = "lambda.zip"
}

variable "custom_bucket" {
  type = "string"
}