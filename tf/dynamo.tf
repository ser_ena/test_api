resource "aws_dynamodb_table" "campaign_data" {
  name            = "${terraform.workspace}_${var.app_name}_campaign_data"
  billing_mode    = "PAY_PER_REQUEST"
  hash_key        = "campaign_id"
  range_key       = "idecran"


  attribute {
    name = "campaign_id"
    type = "S"
  }
  attribute {
    name = "idecran"
    type = "S"
  }
}

resource "aws_dynamodb_table" "schemas" {
  name            = "${terraform.workspace}_${var.app_name}_schemas"
  billing_mode    = "PAY_PER_REQUEST"
  hash_key        = "campaign_id"

  attribute {
    name = "campaign_id"
    type = "S"
  }
}