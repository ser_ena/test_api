#IAM
data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid = "1"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:eu-west-1:*:log-group:/aws/lambda/*:*",
    ]
  }

  statement {
    sid = "2"

    actions = [
      "logs:CreateLogGroup",
    ]

    resources = [
      "arn:aws:logs:eu-west-1:*:*",
    ]
  }

//  statement {
//    sid = "3"
//
//    actions = [
//      "lambda:InvokeAsync",
//      "lambda:InvokeFunction",
//      "lambda:ListFunctions"
//    ]
//
//    resources = [
//      "arn:aws:lambda:eu-west-1:*:*",
//    ]
//  }

  statement {
    sid = "4"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    sid = "5"

    actions = [
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:DescribeTable",
      "dynamodb:DeleteItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:Query"
    ]

    resources = [
      "arn:aws:dynamodb:eu-west-1:*:*"
    ]
  }
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    effect = "Allow"
  }
}

//This data source can be used to fetch information about a specific IAM role.
//By using this data source, you can reference IAM role properties without
//having to hard code ARNs as input.
resource "aws_iam_role" "lambda_role" {
  name = "iam_lambda_${terraform.workspace}-${var.app_name}_role"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_assume.json}"
}

resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "iam_lambda_${terraform.workspace}-${var.app_name}_policy"
  role = "${aws_iam_role.lambda_role.id}"
  policy = "${data.aws_iam_policy_document.lambda_policy.json}"
}

