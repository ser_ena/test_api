resource "aws_lambda_function" "helloWorld" {
  s3_bucket     = "${var.func_bucket}"
  s3_key        = "${var.app_name}/${terraform.workspace}/helloWorld/${var.func_version_helloWorld}/${var.func_name_helloWorld}"
  function_name = "${terraform.workspace}_${var.app_name}_helloWorld"
  role          = "${aws_iam_role.lambda_role.arn}"
  handler       = "handler.handler"
  runtime       = "python3.6"
  timeout       = 20

  environment {
    variables = {
      CAMPAIGN_TABLE = "${aws_dynamodb_table.campaign_data.name}"
      SCHEMAS_TABLE = "${aws_dynamodb_table.schemas.name}"
    }
  }
}
resource "aws_api_gateway_rest_api" "helloWorld" {
  name                = "${terraform.workspace}_${var.app_name}"
  description         = "API for helloWorld test"
}

resource "aws_api_gateway_resource" "helloWorld" {
  rest_api_id = "${aws_api_gateway_rest_api.helloWorld.id}"
  parent_id   = "${aws_api_gateway_rest_api.helloWorld.root_resource_id}"
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "helloWorld" {
  rest_api_id   = "${aws_api_gateway_rest_api.helloWorld.id}"
  resource_id   = "${aws_api_gateway_resource.helloWorld.id}"
  http_method   = "ANY"
  authorization = "NONE"
}

//Provides an HTTP Method Integration for an API Gateway Integration.
resource "aws_api_gateway_integration" "api" {
  rest_api_id = "${aws_api_gateway_rest_api.helloWorld.id}"
  resource_id = "${aws_api_gateway_method.helloWorld.resource_id}"
  http_method = "${aws_api_gateway_method.helloWorld.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.helloWorld.invoke_arn}"
}

resource "aws_api_gateway_deployment" "helloWorld" {
    depends_on = [
      "aws_api_gateway_integration.api",
    ]

    rest_api_id = "${aws_api_gateway_rest_api.helloWorld.id}"
    stage_name  = "${terraform.workspace}"
}

//Creates a Lambda permission to allow external sources invoking the Lambda function (e.g. CloudWatch Event Rule, SNS or S3).
resource "aws_lambda_permission" "helloWorld" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.helloWorld.arn}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_deployment.helloWorld.execution_arn}/*/*"
}

//Connects a custom domain name registered via aws_api_gateway_domain_name with adeployed API
//so that its methods can be called via the custom domain name.
resource "aws_api_gateway_base_path_mapping" "helloWorld" {
  api_id      = "${aws_api_gateway_rest_api.helloWorld.id}"
  stage_name  = "${aws_api_gateway_deployment.helloWorld.stage_name}"
  domain_name = "api2.elise.tech"
  base_path   = "${aws_api_gateway_deployment.helloWorld.stage_name == "production" ? "helloWorld" : replace("helloWorld_env", "env", aws_api_gateway_deployment.helloWorld.stage_name)}"
}

output "apigateway_url" {
  value = "${aws_api_gateway_deployment.helloWorld.invoke_url}"
}